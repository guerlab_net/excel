# guerlab-excel

guerlab excel工具集

## maven仓库地址

```
<dependency>
	<groupId>net.guerlab</groupId>
	<artifactId>guerlab-excel</artifactId>
	<version>1.3.0</version>
</dependency>
```

## 更新记录

### 20180426 v1.3.0

- 更新依赖guerlab-commons 1.2.0 -> 1.3.0
- net.guerlab.excel.Parse增加addAllCellInfo,优化内部实现
- net.guerlab.excel.ParseBuild增加addAllCellInfo方法