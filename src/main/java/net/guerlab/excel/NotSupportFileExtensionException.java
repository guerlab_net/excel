package net.guerlab.excel;

import net.guerlab.commons.exception.ApplicationException;

/**
 * 不支持的文件类型异常
 *
 * @author guer
 *
 */
public class NotSupportFileExtensionException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    /**
     * 根据文件扩展名创建不支持的文件类型异常
     *
     * @param extension
     *            文件扩展名
     */
    public NotSupportFileExtensionException(String extension) {
        super("only support .xls or .xlsx, but the extension is " + extension);
    }

}
