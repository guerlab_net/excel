package net.guerlab.excel;

/**
 * 单元格信息
 *
 * @author guer
 *
 */
public class CellInfo {

    /**
     * 字段名称
     */
    private final String fieldName;

    /**
     * 列ID
     */
    private Integer cellIndex;

    /**
     * 默认值
     */
    private String defaultValue;

    /**
     * 通过字段名称初始化单元格信息
     *
     * @param fieldName
     *            字段名称
     */
    public CellInfo(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * 通过字段名称初始化单元格信息
     *
     * @param fieldName
     *            字段名称
     * @param cellIndex
     *            列ID
     */
    public CellInfo(String fieldName, Integer cellIndex) {
        this.fieldName = fieldName;
        this.cellIndex = cellIndex;
    }

    /**
     * 返回列ID
     *
     * @return 列ID
     */
    public Integer getCellIndex() {
        return cellIndex;
    }

    /**
     * 设置列ID
     *
     * @param cellIndex
     *            列ID
     */
    public void setCellIndex(
            Integer cellIndex) {
        this.cellIndex = cellIndex;
    }

    /**
     * 返回默认值
     *
     * @return 默认值
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * 设置默认值
     *
     * @param defaultValue
     *            默认值
     */
    public void setDefaultValue(
            String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * 返回字段名称
     *
     * @return 字段名称
     */
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CellInfo [fieldName=");
        builder.append(fieldName);
        builder.append(", cellIndex=");
        builder.append(cellIndex);
        builder.append(", defaultValue=");
        builder.append(defaultValue);
        builder.append("]");
        return builder.toString();
    }
}
