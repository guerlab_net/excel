package net.guerlab.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import net.guerlab.commons.time.TimeHelper;

/**
 * Excel助手
 *
 * @author guer
 *
 */
public class ExcelHelp {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0");

    private ExcelHelp() {
    }

    /**
     * 获取文本内容
     *
     * @param sheet
     *            工作表
     * @param rIndex
     *            行号
     * @param cIndex
     *            列号
     * @return 文本内容
     */
    public static String getStringValue(
            Sheet sheet,
            int rIndex,
            int cIndex) {
        return getStringValue(sheet, rIndex, cIndex, null);
    }

    /**
     * 获取文本内容
     *
     * @param sheet
     *            工作表
     * @param rIndex
     *            行号
     * @param cIndex
     *            列号
     * @param decimalFormat
     *            数字格式化
     * @return 文本内容
     */
    public static String getStringValue(
            Sheet sheet,
            int rIndex,
            int cIndex,
            DecimalFormat decimalFormat) {
        if (sheet == null) {
            return null;
        }
        return getStringValue(sheet.getRow(rIndex), cIndex, decimalFormat);
    }

    /**
     * 获取文本内容
     *
     * @param row
     *            行对象
     * @param cIndex
     *            列号
     * @return 文本内容
     */
    public static String getStringValue(
            Row row,
            int cIndex) {
        return getStringValue(row, cIndex, null);
    }

    /**
     * 获取文本内容
     *
     * @param row
     *            行对象
     * @param cIndex
     *            列号
     * @param decimalFormat
     *            数字格式化
     * @return 文本内容
     */
    public static String getStringValue(
            Row row,
            int cIndex,
            DecimalFormat decimalFormat) {
        if (row == null) {
            return null;
        }

        Cell cell = row.getCell(cIndex);

        if (cell == null) {
            return null;
        }

        return getStringValue(cell, decimalFormat);
    }

    private static String getStringValue(
            Cell cell,
            DecimalFormat decimalFormat) {
        switch (cell.getCellTypeEnum()) {
            case NUMERIC:
                return getNumberTypeStringValue(cell, decimalFormat);
            case STRING:
                return cell.getStringCellValue();
            case FORMULA:
                return cell.getCellFormula();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            default:
                return "";
        }
    }

    private static String getNumberTypeStringValue(
            Cell cell,
            DecimalFormat decimalFormat) {
        if (DateUtil.isCellDateFormatted(cell)) {
            return TimeHelper.formatDate(cell.getDateCellValue());
        } else if (decimalFormat != null) {
            return decimalFormat.format(cell.getNumericCellValue());
        } else {
            return DECIMAL_FORMAT.format(cell.getNumericCellValue());
        }
    }

    /**
     * 获取工作薄对象
     *
     * @param file
     *            文件对象
     * @return 工作薄对象
     * @throws IOException
     *             当读取失败的时候抛出IOException异常
     * @throws NotSupportFileExtensionException
     *             当文件类型不支持的时候抛出DoesNotSupportException异常
     */
    public static Workbook getWorkbook(
            File file) throws IOException, NotSupportFileExtensionException {
        if (file == null || !file.isFile()) {
            throw new FileNotFoundException(String.valueOf(file));
        }

        String fileName = file.getName();

        if (fileName.endsWith("xlsx")) {
            return new XSSFWorkbook(new FileInputStream(file));
        } else if (fileName.endsWith("xls")) {
            return new HSSFWorkbook(new FileInputStream(file));
        } else {
            throw new NotSupportFileExtensionException(fileName);
        }
    }
}
