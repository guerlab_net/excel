package net.guerlab.excel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 解析对象构建
 *
 * @author guer
 *
 */
public class ParseBuild {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseBuild.class);

    private int start;

    private final List<CellInfo> cellInfos = new ArrayList<>();

    private ParseBuild() {
    }

    /**
     * 创建一个解析对象构建对象
     *
     * @return 解析对象构建对象
     */
    public static ParseBuild createBuild() {
        return new ParseBuild();
    }

    /**
     * 设置开始行数
     *
     * @param start
     *            开始行数
     * @return 解析对象构建对象
     */
    public ParseBuild setStart(
            int start) {
        this.start = start;
        return this;
    }

    /**
     * 添加单元格信息
     *
     * @param cellInfo
     *            单元格信息
     * @return 解析对象构建对象
     */
    public ParseBuild addCellInfo(
            CellInfo cellInfo) {
        if (cellInfo != null) {
            cellInfos.add(cellInfo);
        }
        return this;
    }

    /**
     * 添加单元格信息集合
     *
     * @param cellInfoCollection
     *            单元格信息集合
     * @return 解析对象构建对象
     */
    public ParseBuild addAllCellInfo(
            Collection<CellInfo> cellInfoCollection) {
        if (cellInfoCollection != null && !cellInfoCollection.isEmpty()) {
            cellInfos.addAll(cellInfoCollection.stream().filter(Objects::nonNull).collect(Collectors.toList()));
        }
        return this;
    }

    /**
     * 构建解析对象
     *
     * @param <T>
     *            解析对象类型
     * @param clazz
     *            解析对象实体类型
     * @return 解析对象
     */
    public <T extends Parse> T build(
            Class<T> clazz) {
        T parse = null;
        try {
            parse = clazz.newInstance();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return null;
        }

        parse.setStart(start);
        parse.addAllCellInfo(cellInfos);

        return parse;
    }
}
